# tmate binaries

tmate is built on top of tmux. tmux and tmate are BSD-licensed. See
[LICENSE.txt](./LICENSE.txt)

The v2.4.0 tmate binaries included in the repo are downloaded from the
[tmate release page](https://github.com/tmate-io/tmate/releases/tag/2.4.0).
