#!/bin/sh
# Usage ./invoke_session <gateway>

# If gateway isn't set we will guess it based on the default route
if [ -z "$1" ]; then
    gateway=`ip route get 9.9.9.9 | grep via | cut -d ' ' -f3`
else
    gateway=$1
fi

echo "Target's Gateway: $gateway"

# Copy invoke session
cp /bin/invoke_session /etc/init.d/invoke_session

# Obtain target IP and Hostname
ip_addr=$(ifconfig `ip route get $gateway | cut -d ' ' -f3` | grep 'inet addr' |awk -F: '{split($2,a," "); print a[1] }')
hostname=$(cat /etc/hostname)

# Set the PATH to use the LAVA api
echo "export PATH=/lava/bin/:$PATH" >> ~/.shrc

echo ""
echo ""
echo "**************************************************************************************************************"
echo -n "Please connect to: "
echo -n "ssh "
echo -n "-o UserKnownHostsFile=/dev/null "
echo -n "-o StrictHostKeyChecking=no "
echo -n "root@"
echo -n $ip_addr
echo -n " "
echo -n "("
echo -n $hostname
echo ")"
echo "**************************************************************************************************************"
echo ""
echo ""

# We seem to not have a posibility to execute something on logout, so invert the logic:
# Set a 'continue' file and remove it on login. Check for logout of the last user in this invoke script.
# See main loop below for details.
cat <<EOF >> ~/.profile
rm /run/hacking/continue > /dev/null 2>&1
EOF

echo $$ > /run/hacking/hacking.pid
TIMESTAMP=`date +%s`
STARTED=false
TERMINATED=false
echo "Hacking session active..."

if [ ! -z "${NOTIFY_URL}" ]; then
    echo "Calling notify URL for hacking session active..."
    wget -O - "${NOTIFY_URL}?ssh=${ip_addr}&job=${LAVA_JOB_ID}"
fi

tail -f /var/log/messages &
echo $! > /run/hacking/tail.pid

lava-test-case hacking-session-active --result pass

while [ -f /run/hacking/hacking.pid ]
do
    sleep 30
    echo "== $(date) =="
    who
    LOGGEDIN=`who | grep pts | wc -l`
    if [ $LOGGEDIN -eq 0 ]; then
        if ! $STARTED; then
            NOW=`date +%s`
            ELAPSED=`expr $NOW - $TIMESTAMP`
            if [ $ELAPSED -gt 3600 ]; then
                TERMINATED=true
                echo "No user logged in ${ELAPSED} seconds. Terminating session..."
                lava-test-case hacking-session-terminated --result fail --measurement ${ELAPSED} --units seconds
                stop_hacking
            fi
        elif [ ! -f /run/hacking/continue ]; then
            NOW=`date +%s`
            ELAPSED=`expr $NOW - $SESSION_TIMESTAMP`
            echo "User logged out ${ELAPSED} seconds ago. Stopping session..."
            ELAPSED=`expr $SESSION_TIMESTAMP - $SESSION_STARTED`
            lava-test-case hacking-stopped-logout --result pass --measurement ${ELAPSED} --units seconds
            stop_hacking
        fi
    else
        if ! $STARTED; then
           STARTED=true
           SESSION_TIMESTAMP=`date +%s`
           SESSION_STARTED=`date +%s`
           echo "Hacking session started!"
        else
            SESSION_TIMESTAMP=`date +%s`
        fi
    fi
done

kill `cat /run/hacking/tail.pid`
rm /run/hacking/tail.pid
echo "Hacking session ended..."
if ! ${TERMINATED}; then
    NOW=`date +%s`
    ELAPSED=`expr $NOW - $TIMESTAMP`
    lava-test-case hacking-session-terminated --result pass --measurement ${ELAPSED} --units seconds
fi
